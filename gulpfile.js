var gulp   = require('gulp'),
sass   = require('gulp-sass'),
uglify   = require('gulp-uglify'),
nunjucksRender = require('gulp-nunjucks-render'),
bower = require('gulp-bower')


gulp.task('default', ['watch']);

gulp.task('watch', function() {
  gulp.watch('src/scss/**/*.scss', ['build_css']);
  gulp.watch('src/javascript/**/*.js', ['build_js']);
  gulp.watch('src/image/**/*', ['move_images']);
  gulp.watch('src/**/*.+(html|nunjucks)', ['build_html']);
  gulp.watch('src/fonts/**/*', ['move_fonts']);
  gulp.watch('./bower_components', ['move_bower_components']);
});

// compile scss
gulp.task('build_css', function() {
  return gulp.src('src/scss/**/*.scss')
    .pipe(sass())
    .pipe(gulp.dest('public/stylesheets'));
});

gulp.task('build_js', function() {
  return gulp.src('src/javascript/**/*.js')
    .pipe(uglify())
    .pipe(gulp.dest('public/javascript'));
});

gulp.task('move_images', function () {
  return gulp.src('src/image/**/*')
             .pipe(gulp.dest('public/image'));
});

gulp.task('build_html', function () {
  return gulp.src('src/**/*.+(html|nunjucks)')
    .pipe(nunjucksRender({
      path: ['src/templates/'] // String or Array
    }))
    .pipe(gulp.dest('public'));
});

gulp.task('move_bower_components', function() {
    return bower().pipe(gulp.dest('public/bower_components'));
});

gulp.task('move_fonts', function() {
  return gulp.src('src/fonts/**/*')
             .pipe(gulp.dest('public/fonts'));
});

gulp.task('build', ['build_html','build_js', 'build_css', 'move_bower_components', 'move_images', 'move_fonts']);